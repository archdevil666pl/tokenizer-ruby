# Tokenizer2fa::Client

This is an integration SDK for [tokenizer.com](https://tokenizer.com) service. The SDK is framework independent and works with pure Ruby 1.9 (or above).

Tokenizer is a cloud-based Two-Factor authentication solution developed by [Amsterdam Standard](http://amsterdamstandard.com).

## Installation

Add this line to your application's Gemfile:

    gem 'tokenizer2fa-client'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tokenizer2fa-client

## Usage

#### Configuration
In order to connect with Tokenizer, we need the APP ID and APP KEY that you receive via email, when creating a Tokenizer service.
```ruby
tokenizer_config = {
    app_id:  'YOUR-APP-ID',
    app_key: 'YOUR-APP-KEY',
    host:    'https://api.tokenizer.com/'
}
tokenizer = Tokenizer2fa::Client.new tokenizer_config
```

#### Creating a token
```ruby
id = tokenizer.create_auth 'email@example.com'
```

#### Verifying a token
```ruby
auth_state = tokenizer.verify_auth id
puts auth_state.to_s
```

#### Polling token verification
This gem does not use the Tokenizer provided waiting page, but allows you to poll the API whilst waiting for the user to Tokenize.
```ruby
auth_state = tokenizer.poll_verify_auth id
if auth_state.is_accepted?
  puts 'Authenticated'
else
  puts 'Denied'
end
```

## Contributing

1. Fork it ( https://bitbucket.org/tokenizer-integrations/tokenizer-ruby/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
