module Tokenizer2fa::Token
  class State

    def initialize state
      @state = state
    end

    def is_pending?
      @state == 'pending'
    end

    def is_accepted?
      @state == 'accepted'
    end

    def to_s
      @state
    end

  end
end