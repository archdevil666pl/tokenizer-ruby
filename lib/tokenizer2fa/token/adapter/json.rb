require 'json'

module Tokenizer2fa::Token::Adapter
  class Json

    def parse data
      JSON.parse(data)
    end

  end
end