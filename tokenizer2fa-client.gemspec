# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tokenizer2fa/version'

Gem::Specification.new do |spec|
  spec.name          = "tokenizer2fa-client"
  spec.version       = Tokenizer2fa::VERSION
  spec.authors       = ["Radosław Fąfara"]
  spec.email         = ["radek@amsterdam-standard.pl"]
  spec.summary       = 'Tokenizer2fa is an integration client for tokenizer.com service.'
  spec.description   = 'The SDK allows framework independent two-factor authentication for your web application. Client handles both token generation and validation.'
  spec.homepage      = "https://bitbucket.org/tokenizer-integrations/tokenizer-ruby"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]
  spec.required_ruby_version = '>= 1.9'

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "capybara"
  spec.add_development_dependency "sinatra"
  spec.add_development_dependency "webmock"
end
